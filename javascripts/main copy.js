$(document).ready(function() {
	/*fullpage js with velocity animation
	$('#fullpage').fullpage({
		afterLoad: function(anchorLink, index){
            var loadedSection = $(this);
            //Section 1 loaded and give velocity animation
            if(".fp-viewing-0"){
                $(".we")
                    .velocity("transition.flipBounceYIn", {duration: 2000, stagger: 250, drag: true});
                $(".tagline-home")
                    .velocity("transition.flipBounceYIn", {duration: 2000, stagger: 250, drag: true});
            }
        },
        afterSlideLoad: function(anchorLink, index) {
            var loadedSection = $(this);
            //Section 1 loaded and give velocity animation
            if(".fp-viewing-1"){
                

  $('.svg-wraper').slimScroll(function() {
    drawLine( $('#route'),
              document.getElementById('path') );
  });
  
  // init the line length
    drawLine( $('#route'),
              document.getElementById('path') );

  //draw the line
  function drawLine(container, line){
    
    var pathLength = line.getTotalLength(),
        maxScrollTop = $(document).height() - $(window).height(),
        percentDone = $(window).scrollTop() / maxScrollTop,
        length = percentDone * pathLength;
    line.style.strokeDasharray = [ length ,pathLength].join(' ');
  }
            }
        },
        scrollOverflow: true,

	});  
    */
    $(function() {
        $.scrollify({
            section : ".section",
            after: function(anchorLink, index){
            var loadedSection = $(this);
            //Section 1 loaded and give velocity animation
            if("#top-section"){
                $(".we")
                    .velocity("transition.flipBounceYIn", {duration: 2000, stagger: 250, drag: true});
                $(".tagline-home")
                    .velocity("transition.flipBounceYIn", {duration: 2000, stagger: 250, drag: true});
            }
        },
        before: function(anchorLink, index) {
            var loadedSection = $(this);
            if('#main-section') {
                    $(window).scroll(function() {
                    drawLine( $('#route'),
                    document.getElementById('path') );
                });
            // init the line length
                drawLine( $('#route'),
                document.getElementById('path') );
            //draw the line
                function drawLine(container, line){
                    var pathLength = line.getTotalLength(),
                    maxScrollTop = $(document).height() - $(window).height(),
                    percentDone = $(window).scrollTop() / maxScrollTop / 1.2, 
                    length = percentDone * pathLength;
                    line.style.strokeDasharray = [ length ,pathLength].join(' ');
                };
            }
        }
        });
    });
    //change hello text
    var items = ["HALO,", "HEJ,", "Olá,", "こんにちは,","नमस्ते,","สวัสดี,",  "HELLO,"],
        $text = $( '.hello h3' ),
        delay = 1.2; // delay animation seconds
    //showing first hello    
    $text.velocity("transition.fadeIn", {duration: 1000, stagger: 250, drag: false});
    //change looping with array
    function loop ( delay ) {
        $.each( items, function ( i, elm ){
            $text.delay( delay*1E3).velocity("transition.fadeOut", {duration: 500, stagger: 250, drag: false});
            $text.queue(function(){
                $text.html( items[i] );
                $text.dequeue();
            });
            $text.velocity("transition.fadeIn", {duration: 500, stagger: 250, drag: false});
            $text.queue(function(){
                if ( i == items.length -1 ) {
                    loop(delay);   
                }
                $text.dequeue();
            });
        });
    }

    loop( delay );




	
});

