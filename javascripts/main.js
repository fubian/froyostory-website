$(document).ready(function() {
    //velocity laoded
    $(".we").velocity("transition.flipBounceYIn", {duration: 2000, stagger: 250, drag: true});
    $(".tagline-home").velocity("transition.flipBounceYIn", {duration: 2000, stagger: 250, drag: true});
    $(".rect").delay(500)
    .velocity({width:100});

    $(function() {
        $.scrollify({
            section : ".section",
            easing: "easeInOutQuart",
            offset: 10,
            scrollSpeed: 1100
            /*
            after: function(anchorLink, index){
            var loadedSection = $(this);
            //Section 1 loaded and give velocity animation
            if("#top-section"){
                $(".we")
                    .velocity("transition.flipBounceYIn", {duration: 2000, stagger: 250, drag: true});
                $(".tagline-home")
                    .velocity("transition.flipBounceYIn", {duration: 2000, stagger: 250, drag: true});
            }
        },
        /* hidden for next
        before: function(anchorLink, index) {
            var loadedSection = $(this);
            if('#main-section') {
                    $(window).scroll(function() {
                    drawLine( $('#route'),
                    document.getElementById('path') );
                });
            // init the line length
                drawLine( $('#route'),
                document.getElementById('path') );
            //draw the line
                function drawLine(container, line){
                    var pathLength = line.getTotalLength(),
                    maxScrollTop = $(document).height() - $(window).height(),
                    percentDone = $(window).scrollTop() / maxScrollTop / 1.2, 
                    length = percentDone * pathLength;
                    line.style.strokeDasharray = [ length ,pathLength].join(' ');
                };
            }
        }*/
        }); 
    });
    //change hello text
    var items = ["HALO,", "HEJ,", "Olá,", "こんにちは,","नमस्ते,","สวัสดี,",  "HELLO,"],
        $text = $( '.hello h3' ),
        delay = 1.2; // delay animation seconds
    //showing first hello    
    $text.velocity("transition.fadeIn", {duration: 1000, stagger: 250, drag: false});
    //change looping with array
    function loop ( delay ) {
        $.each( items, function ( i, elm ){
            $text.delay( delay*1E3).velocity("transition.fadeOut", {duration: 500, stagger: 250, drag: false});
            $text.queue(function(){
                $text.html( items[i] );
                $text.dequeue();
            });
            $text.velocity("transition.fadeIn", {duration: 500, stagger: 250, drag: false});
            $text.queue(function(){
                if ( i == items.length -1 ) {
                    loop(delay);   
                }
                $text.dequeue();
            });
        });
    }

    loop( delay );

// init skroll.js
    var s = skrollr.init({
        forceHeight: false
    });
//smooth scroll velocity
  // bind click event to all internal page anchors
    $('a[href*="#"]').on('click', function(e) {
        // prevent default action and bubbling
        $.scrollify.instantNext();
        e.preventDefault();
        e.stopPropagation();
        // set target to anchor's "href" attribute
        var target = $(this).attr('href');
        // scroll to each target
        $(target).velocity('scroll', {
            duration: 800,
            offset: -40,
            easing: 'ease-in-out'
        });

    });

	
});

